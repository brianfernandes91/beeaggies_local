//Function to load page parts
function insertPagePart()
{
	var pageParamName = getUrlParam('page');
	var page='';
	if(pageParamName)
	{
		//If parameter present, show appropriate page
		page="pageparts/part"+pageParamName+".html";
	}
	else
	{
		//If parameter not present, show index.html (home) page
		page="pageparts/partindex.html";
	}
	if($("#placeholder"))
	{
		$("#placeholder").load(page, function(response, status, xhr) {
			if (status == "error") {
				//window.location.href="404.html";
			}
		});
	}
}

$(document).ready(function() {
    insertPagePart();
});

//Other page specific functions
function collapseNav(){
	
	var navElement = document.getElementById("menuBar");
	if(navElement.className == "navBar"){
		navElement.className+=" responsive";
	}
	else{
		navElement.className= "navBar";
	}
}


//Utility functions
//Function for getting querystring parameter
function getUrlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}